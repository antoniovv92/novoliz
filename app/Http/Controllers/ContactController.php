<?php

namespace App\Http\Controllers;
use App\Mail\Contact;
use Mail;
use Illuminate\Http\Request;

// use App\Http\Requests;

class ContactController extends Controller
{
  /**
   * Fetch and display contact form
   * @return [view] [description]
   */
  public function create()
  {
     return view("contact");
  }

  public function contact(Request $request)
  {
    /*$this->validate(request(), [
      'first_name'=>'required',
      'last_name'=>'required',
      'email'=>'required',
      'subject'=>'required',
      'message'=>'required',
    ]);*/

    // the next step here is to actually send an email to some end point email
    // maybe you wanna send a copy to website admin -- moderator  whatever you get the idea

     // finally tell us that you are succeeded
    
    //error al usar input('message'), esta siendo tomado como objeto, cambiado a input('msg')
    Mail::to(env('MAIL_TO'))->send(new Contact($request->input('name'),$request->input('email'),$request->input('phone'),$request->input('msg')));
    
    return ['message'=>'project created'];
  }
}
