
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Prueba email de contacto</title>
</head>
<body>
    <p>Nombre: {{ $name }}</p>
    <p>Correo: {{ $email }}</p>
    <p>Teléfono: {{ $phone }}</p>
    <p>Mensaje: {{ $msg }}</p>
</body>
</html>