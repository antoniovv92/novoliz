export const linksMenu = [
  {
    link: 'HomePage',
    name: 'INICIO'
  },
  {
    link: 'AboutPage',
    name: 'NOSOTROS'
  },
  {
    link: 'ProductsPage',
    name: 'PRODUCTOS'
  },
  {
    link: 'GalleryPage',
    name: 'GALERÍA'
  },
  {
    link: 'ContactPage',
    name: 'CONTACTO'
  }
]
